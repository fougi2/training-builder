pub mod training;

pub mod style {
    use iced::Color;

    pub const TEXT_INPUT_WIDTH_LENGTH: iced::Length = iced::Length::Units(200);
    pub const VALUE_INPUT_WIDTH_LENGTH: iced::Length = iced::Length::Units(60);
    pub const INPUT_PADDING: u16 = 5;
    pub const INPUT_SIZE: u16 = 20;

    pub enum TextInput {
        Main,
    }

    impl iced::text_input::StyleSheet for TextInput {
        fn active(&self) -> iced::text_input::Style {
            iced::text_input::Style {
                border_radius: 0.0,
                border_width: 0.0,
                border_color: Color::TRANSPARENT,
                ..iced::text_input::Style::default()
            }
        }

        fn focused(&self) -> iced::text_input::Style {
            iced::text_input::Style {
                border_radius: 0.5,
                border_width: 1.0,
                border_color: Color::BLACK,
                ..iced::text_input::Style::default()
            }
        }

        fn placeholder_color(&self) -> Color {
            Color::from_rgb(0.7, 0.7, 0.7)
        }

        fn value_color(&self) -> Color {
            Color::from_rgb(0.3, 0.3, 0.3)
        }

        fn selection_color(&self) -> Color {
            Color::from_rgb(0.8, 0.8, 1.0)
        }
    }
}


#[cfg(test)]
mod tests {
    #[test]
    fn pdf_generation() {
        assert!(true);
    }
}
