use iced::{
    pure::{
        widget::{Button, Column, Container, PickList, Row, Scrollable, TextInput},
        Sandbox,
    },
    Settings,
};
use training_builder_tb::style::{
    self, INPUT_PADDING, INPUT_SIZE, TEXT_INPUT_WIDTH_LENGTH, VALUE_INPUT_WIDTH_LENGTH,
};
use training_builder_tb::training::{Exercise, ExerciseBlock, Intensity, Training, TypeExec};
use uuid::Uuid;

fn main() -> Result<(), iced::Error> {
    TrainingState::run(Settings::default())
}

#[derive(Default)]
struct TrainingState {
    current_training: Training,
}

#[derive(Debug, Clone)]
    enum TrainingMessage {
        AddExoBlock,
        AddExo(Uuid),
        ExoNameChanged(Uuid, String),
        ExoTypeExecChange(Uuid, TypeExec),
        ChangeTypeExecValue(Uuid, String),
        ExoIntensityChange(Uuid, Intensity),
        ChangeIntensityValue(Uuid, String),
        DuplicateExo(Uuid),
        ExoBlockRepeatChange(Uuid, String),
        RemoveExoBlock(Uuid),
        RemoveExo(Uuid),
        GeneratePdf
    }

impl Sandbox for TrainingState {
    type Message = TrainingMessage;

    fn new() -> Self {
        TrainingState {
            current_training: Training::new(),
        }
    }

    fn title(&self) -> String {
        String::from("Training Builder")
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            TrainingMessage::AddExoBlock => {
                self.current_training.add_ex_block(ExerciseBlock::new(1));
            }
            TrainingMessage::AddExo(exo_block_id) => {
                let exos_count = self.current_training.get_exos_count();
                let exo = Exercise::new(format!("Ex #{}", exos_count));
                self.current_training.add_exo(&exo_block_id, exo);
            }
            TrainingMessage::ExoNameChanged(exo_id, new_name) => {
                self.current_training.set_exo_name(&exo_id, new_name);
            }
            TrainingMessage::ExoTypeExecChange(exo_id, type_exec) => {
                self.current_training.set_exo_type_exec(&exo_id, type_exec)
            }
            TrainingMessage::ChangeTypeExecValue(exo_id, new_value) => {
                let new_type_exec_value = new_value.parse::<i32>().unwrap_or(0);
                // Check if string end without a number
                    // If not, keep value without last char
                self.current_training
                    .set_exo_type_exec_value(&exo_id, new_type_exec_value)
            }
            TrainingMessage::ExoIntensityChange(exo_id, intensity) => {
                self.current_training.set_exo_intensity(&exo_id, intensity)
            }
            TrainingMessage::ChangeIntensityValue(exo_id, new_value) => {
                let new_intensity_value = new_value.parse::<f32>().unwrap_or(0.0);
                // Check if string end with "." or ","
                    // If so, add a 0 at the end
                    // If not, keep value without last char
                self.current_training
                    .set_exo_intensity_value(&exo_id, new_intensity_value)
            }
            TrainingMessage::DuplicateExo(exo_id) => {
                self.current_training.duplicate_exo(&exo_id);
            }
            TrainingMessage::ExoBlockRepeatChange(exo_block_id, new_value) => {
                let new_repeat_value = new_value.parse::<i32>().unwrap_or(0);

                self.current_training
                    .set_exo_block_repeat(&exo_block_id, new_repeat_value);
            }
            TrainingMessage::RemoveExoBlock(exo_block_id) => {
                self.current_training.remove_exo_block(&exo_block_id);
            }
            TrainingMessage::RemoveExo(exo_id) => {
                self.current_training.remove_exo(&exo_id);
            }
            TrainingMessage::GeneratePdf => {
                self.current_training.generate_doc();
            }
        }
    }

    fn view(&self) -> iced::pure::Element<'_, Self::Message> {
        let exo_blocks = self
            .current_training
            .get_exo_blocks()
            .iter()
            .fold(Column::new().spacing(40), |column, eb| {
                column.push(eb.view())
            });
        
        let generate_pdf = Column::new()
            .push(Button::new("Generate PDF").on_press(TrainingMessage::GeneratePdf))
            .width(iced::Length::Units(900))
            .align_items(iced::Alignment::End);

        let col = Column::new()
            .push(exo_blocks)
            .push(Button::new("Add Exo Block").on_press(TrainingMessage::AddExoBlock))
            .width(iced::Length::Fill)
            .spacing(5)
            .align_items(iced::Alignment::Center);

        let row = Column::new()
            .push(col)
            .push(generate_pdf)
            .align_items(iced::Alignment::Start);

        Container::new(Scrollable::new(row))
            .center_x()
            .center_y()
            .width(iced::Length::Fill)
            .into()
    }
}

trait Displayable {
    fn view(&self) -> iced::pure::Element<TrainingMessage>;
}

impl Displayable for ExerciseBlock {
    fn view(&self) -> iced::pure::Element<TrainingMessage> {
        let exos = self
            .get_exos()
            .iter()
            .fold(Column::new(), |column, exo| column.push(exo.view()));

        let col_delete = Column::new()
            .push(Button::new("X").on_press(TrainingMessage::RemoveExoBlock(self.get_id())))
            .width(iced::Length::Units(900))
            .align_items(iced::Alignment::End);

        let row_exos = Row::new().push(exos).width(iced::Length::Fill);

        let row_block = Row::new()
            .push(Button::new("Add Exo").on_press(TrainingMessage::AddExo(self.get_id())))
            .push(
                TextInput::new("", &self.get_repeat().to_string(), |new_val| {
                    TrainingMessage::ExoBlockRepeatChange(self.get_id(), new_val)
                })
                .width(VALUE_INPUT_WIDTH_LENGTH)
                .padding(INPUT_PADDING)
                .size(INPUT_SIZE)
                .style(style::TextInput::Main),
            )
            .width(iced::Length::Shrink)
            .spacing(5);

        let col = Column::new()
            .push(col_delete)
            .push(row_exos)
            .push(row_block)
            .width(iced::Length::Shrink)
            .align_items(iced::Alignment::Center);

        Container::new(col)
            .center_x()
            .center_y()
            .width(iced::Length::Shrink)
            .height(iced::Length::Shrink)
            .padding(5)
            .into()
    }
}

impl Displayable for Exercise {
    fn view(&self) -> iced::pure::Element<TrainingMessage> {
        let row = Row::new()
            .push(
                TextInput::new("Training Name", self.get_name(), |new_val| {
                    TrainingMessage::ExoNameChanged(self.get_id(), new_val)
                })
                .width(TEXT_INPUT_WIDTH_LENGTH)
                .style(style::TextInput::Main)
                .padding(INPUT_PADDING)
                .size(INPUT_SIZE),
            )
            .push(PickList::new(
                &TypeExec::ALL[..],
                Some(self.get_type_exec()),
                |new_val| TrainingMessage::ExoTypeExecChange(self.get_id(), new_val),
            ))
            .push(
                TextInput::new(
                    &format!("#{}", self.get_type_exec()),
                    &self.get_type_exec().get_value().to_string(),
                    |new_val| TrainingMessage::ChangeTypeExecValue(self.get_id(), new_val),
                )
                .width(VALUE_INPUT_WIDTH_LENGTH)
                .padding(INPUT_PADDING)
                .size(INPUT_SIZE)
                .style(style::TextInput::Main),
            )
            .push(PickList::new(
                &Intensity::ALL[..],
                Some(self.get_intensity()),
                |new_val| TrainingMessage::ExoIntensityChange(self.get_id(), new_val),
            ))
            .push(
                TextInput::new(
                    &format!("#{}", self.get_intensity()),
                    &self.get_intensity().get_value().to_string(),
                    |new_val| TrainingMessage::ChangeIntensityValue(self.get_id(), new_val),
                )
                .width(VALUE_INPUT_WIDTH_LENGTH)
                .padding(INPUT_PADDING)
                .size(INPUT_SIZE)
                .style(style::TextInput::Main),
            )
            .push(Button::new("Duplicate").on_press(TrainingMessage::DuplicateExo(self.get_id())))
            .push(Button::new("X").on_press(TrainingMessage::RemoveExo(self.get_id())))
            .spacing(5);

        Container::new(row)
            .center_x()
            .center_y()
            .width(iced::Length::Fill)
            .height(iced::Length::Units(40))
            .into()
    }
}
