use chrono::{DateTime, Utc};
use printpdf::*;
use std::io::BufWriter;
use std::vec;
use std::{fs::File};
use uuid::Uuid;

#[derive(Default, Clone, Debug)]
pub struct Training {
    ex_blocks: Vec<ExerciseBlock>,
}

impl Training {
    pub fn new() -> Training {
        Training { ex_blocks: vec![] }
    }

    pub fn generate_doc(&self) {
        let now: DateTime<Utc> = Utc::now();
        let title = format!("Training_{}", now);

        let (doc, page1, layer1) = PdfDocument::new(&title, Mm(210.0), Mm(297.0), "Layer 1");
        let current_layer = doc.get_page(page1).get_layer(layer1);

        let font = doc.add_builtin_font(BuiltinFont::Helvetica).unwrap();

        let labor_text_font_size: f64 = 14.0;
        let title_font_size: f64 = 24.0;

        current_layer.set_font(&font, labor_text_font_size);
        current_layer.set_text_cursor(Mm(5.0), Mm(280.0));
        current_layer.set_line_height(labor_text_font_size);
        current_layer.set_word_spacing(2.0);
        current_layer.set_character_spacing(1.2);

        for exo_block in self.ex_blocks.iter() {
            let exo_block_line = format!("Repeat {}", exo_block.get_repeat().to_string());
            current_layer.write_text(exo_block_line, &font);
            current_layer.add_line_break();
            for exo in exo_block.get_exos().iter() {
                let (type_exec, type_exec_val) = match exo.get_type_exec() {
                    TypeExec::Repeat(val) => (String::from("Repeat"), val),
                    TypeExec::Duration(val) => (String::from("Duration"), val),
                    TypeExec::Distance(val) => (String::from("Distance"), val),
                };

                let (intensity, intensity_val) = match exo.get_intensity() {
                    Intensity::Weight(val) => (String::from("Weight"), val),
                    Intensity::Speed(val) => (String::from("Speed"), val),
                };

                let exo_line = format!(
                    "   {} - {} {} - {} {}", 
                    exo.get_name(), 
                    type_exec, 
                    type_exec_val.to_string(), 
                    intensity, 
                    intensity_val.to_string()
                );
                current_layer.write_text(exo_line, &font);
                current_layer.add_line_break();
            }
            current_layer.add_line_break();
            current_layer.add_line_break();
        }

        doc.save(&mut BufWriter::new(
            File::create(format!("{}.pdf", &title)).unwrap(),
        ))
        .unwrap();
    }

    // Region Block
    pub fn add_ex_block(&mut self, ex_block_to_add: ExerciseBlock) {
        self.ex_blocks.push(ex_block_to_add);
    }

    pub fn add_exo(&mut self, exo_block_id: &Uuid, exo_to_add: Exercise) {
        self.get_exo_block(exo_block_id)
            .unwrap()
            .add_exo(exo_to_add);
    }

    pub fn get_exo_blocks(&self) -> &Vec<ExerciseBlock> {
        &self.ex_blocks
    }

    pub fn get_exo_block(&mut self, uuid: &Uuid) -> Option<&mut ExerciseBlock> {
        self.ex_blocks.iter_mut().find(|eb| eb.id.eq(&uuid))
    }

    fn get_block_from_exo_id(&mut self, exo_id: &Uuid) -> Option<&mut ExerciseBlock> {
        self.ex_blocks
            .iter_mut()
            .find(|eb| eb.exos.iter().filter(|exo| exo.id.eq(&exo_id)).count() > 0)
    }

    pub fn set_exo_block_repeat(&mut self, exo_block_id: &Uuid, new_repeat_value: i32) {
        let exo_block = self.get_exo_block(exo_block_id).unwrap();
        exo_block.set_repeat(new_repeat_value);
    }

    pub fn remove_exo_block(&mut self, exo_block_id: &Uuid) {
        let idx = self
            .get_exo_blocks()
            .iter()
            .position(|eb| eb.id.eq(&exo_block_id))
            .unwrap();
        self.ex_blocks.remove(idx);
    }

    // Region Exo
    fn get_exos(&mut self) -> Vec<&mut Exercise> {
        let mut exos: Vec<&mut Exercise> = vec![];
        for eb in self.ex_blocks.iter_mut() {
            eb.exos.iter_mut().for_each(|ex| exos.push(ex));
        }

        exos
    }

    pub fn get_exos_count(&self) -> usize {
        let mut exos: Vec<&Exercise> = vec![];
        for eb in self.ex_blocks.iter() {
            eb.exos.iter().for_each(|ex| exos.push(ex));
        }

        exos.len()
    }

    pub fn set_exo_name(&mut self, uuid: &Uuid, new_name: String) {
        let mut exos = self.get_exos();

        let exo = exos.iter_mut().find(|ex| ex.id.eq(&uuid)).unwrap();

        exo.name = new_name;
    }

    pub fn set_exo_type_exec(&mut self, uuid: &Uuid, new_type_exec: TypeExec) {
        let mut exos = self.get_exos();

        let exo = exos.iter_mut().find(|ex| ex.id.eq(&uuid)).unwrap();

        exo.set_type_exec(new_type_exec);
    }

    pub fn set_exo_type_exec_value(&mut self, uuid: &Uuid, new_type_exec_value: i32) {
        let mut exos = self.get_exos();

        let exo = exos.iter_mut().find(|ex| ex.id.eq(&uuid)).unwrap();

        exo.set_type_exec_value(new_type_exec_value);
    }

    pub fn set_exo_intensity(&mut self, uuid: &Uuid, new_intensity: Intensity) {
        let mut exos = self.get_exos();

        let exo = exos.iter_mut().find(|ex| ex.id.eq(&uuid)).unwrap();

        exo.set_intensity(new_intensity);
    }

    pub fn set_exo_intensity_value(&mut self, uuid: &Uuid, new_intensity_value: f32) {
        let mut exos = self.get_exos();

        let exo = exos.iter_mut().find(|ex| ex.id.eq(&uuid)).unwrap();

        exo.set_intensity_value(new_intensity_value);
    }

    pub fn duplicate_exo(&mut self, exo_id: &Uuid) {
        let mut exos = self.get_exos();

        let exo = exos.iter_mut().find(|ex| ex.id.eq(&exo_id)).unwrap();

        let new_exo =
            Exercise::new_parametered(exo.name.clone(), exo.get_type_exec(), exo.get_intensity());

        self.get_block_from_exo_id(exo_id).unwrap().add_exo(new_exo);
    }

    pub fn remove_exo(&mut self, exo_id: &Uuid) {
        // Get exo_block
        let exo_block = self.get_block_from_exo_id(&exo_id).unwrap();
        // Get idx of exo
        let idx = exo_block
            .get_exos()
            .iter()
            .position(|ex| ex.id.eq(&exo_id))
            .unwrap();
        // remove exo from block
        exo_block.remove_exo(idx);
    }
}

#[derive(Default, Clone, Debug)]
pub struct ExerciseBlock {
    id: Uuid,
    exos: Vec<Exercise>,
    nb_repeat: i32,
}

impl ExerciseBlock {
    pub fn new(nb_repeat: i32) -> ExerciseBlock {
        ExerciseBlock {
            id: Uuid::new_v4(),
            exos: vec![],
            nb_repeat,
        }
    }

    pub fn get_exos(&self) -> &Vec<Exercise> {
        &self.exos
    }

    fn add_exo(&mut self, exo_to_add: Exercise) {
        self.exos.push(exo_to_add);
    }

    pub fn get_id(&self) -> Uuid {
        self.id
    }

    pub fn get_repeat(&self) -> i32 {
        self.nb_repeat
    }

    fn set_repeat(&mut self, new_repeat: i32) {
        self.nb_repeat = new_repeat;
    }

    fn remove_exo(&mut self, exo_idx: usize) {
        self.exos.remove(exo_idx);
    }
}

#[derive(Clone, Debug)]
pub struct Exercise {
    name: String,
    id: Uuid,
    type_exec: TypeExec,
    intensity: Intensity,
}

impl Exercise {
    pub fn new(name: String) -> Self {
        Exercise {
            name,
            id: Uuid::new_v4(),
            type_exec: TypeExec::default(),
            intensity: Intensity::default(),
        }
    }

    fn new_parametered(name: String, type_exec: TypeExec, intensity: Intensity) -> Self {
        Exercise {
            name,
            id: Uuid::new_v4(),
            type_exec,
            intensity,
        }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_id(&self) -> Uuid {
        self.id
    }

    pub fn get_type_exec(&self) -> TypeExec {
        self.type_exec.clone()
    }

    fn set_type_exec(&mut self, type_exec: TypeExec) {
        self.type_exec = type_exec
    }

    fn set_type_exec_value(&mut self, type_exec_value: i32) {
        self.type_exec = match self.type_exec {
            TypeExec::Duration(_) => TypeExec::Duration(type_exec_value),
            TypeExec::Repeat(_) => TypeExec::Repeat(type_exec_value),
            TypeExec::Distance(_) => TypeExec::Distance(type_exec_value),
        }
    }

    pub fn get_intensity(&self) -> Intensity {
        self.intensity.clone()
    }

    fn set_intensity(&mut self, intensity: Intensity) {
        self.intensity = intensity
    }

    fn set_intensity_value(&mut self, intensity_value: f32) {
        self.intensity = match self.intensity {
            Intensity::Weight(_) => Intensity::Weight(intensity_value),
            Intensity::Speed(_) => Intensity::Speed(intensity_value),
        }
    }
}

#[derive(Clone, Debug, Eq)]
pub enum TypeExec {
    Repeat(i32),
    Duration(i32),
    Distance(i32),
}

impl TypeExec {
    pub const ALL: [TypeExec; 3] = [TypeExec::Repeat(0), TypeExec::Duration(0), TypeExec::Distance(0)];

    pub fn get_value(&self) -> i32 {
        match self {
            TypeExec::Duration(a) => *a,
            TypeExec::Repeat(a) => *a,
            TypeExec::Distance(a) => *a,
        }
    }
}

impl Default for TypeExec {
    fn default() -> Self {
        TypeExec::Duration(0)
    }
}

impl std::fmt::Display for TypeExec {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TypeExec::Duration(_) => "Duration",
                TypeExec::Repeat(_) => "Repeat",
                TypeExec::Distance(_) => "Distance",
            }
        )
    }
}

impl PartialEq for TypeExec {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (TypeExec::Duration(a), TypeExec::Duration(b))
            | (TypeExec::Distance(a), TypeExec::Distance(b))
            | (TypeExec::Repeat(a), TypeExec::Repeat(b)) => a == b,
            _ => false,
        }
    }
}

#[derive(Clone, Debug)]
pub enum Intensity {
    Weight(f32),
    Speed(f32),
}

impl Intensity {
    pub const ALL: [Intensity; 2] = [Intensity::Weight(0.0), Intensity::Speed(0.0)];

    pub fn get_value(&self) -> f32 {
        match self {
            Intensity::Weight(a) => *a,
            Intensity::Speed(a) => *a,
        }
    }
}

impl Default for Intensity {
    fn default() -> Self {
        Intensity::Weight(0.0)
    }
}

impl std::fmt::Display for Intensity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Intensity::Weight(_) => "Weight",
                Intensity::Speed(_) => "Speed",
            }
        )
    }
}

impl PartialEq for Intensity {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Intensity::Weight(a), Intensity::Weight(b))
            | (Intensity::Speed(a), Intensity::Speed(b)) => a == b,
            _ => false,
        }
    }
}

impl Eq for Intensity {}
