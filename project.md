# Training Builder
Application de gestion d'entrainement. L'utilisateur peut créer, partager et exporter ses entrainement et les consulter sur une application mobile ou web.

## Structure
L'application est basée sur le développement d'une librairie permettant la gestion des entrainements et pouvant être intégrée dans des applications tiers. Cette approche permet de créer à partir de la même core librairie des applications sur différentes plateformes : web, desktop et mobile. 

![](image1.png)

## Objectifs
- Prendre en main le langague et l'ecosystème Rust
- Découvrir les possibilité et cas d'utilisation de Web Assembly
- Développer un projet Open Source